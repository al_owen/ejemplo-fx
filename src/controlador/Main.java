package controlador;

import java.util.Locale;
import java.util.ResourceBundle;

import org.controlsfx.control.CheckComboBox;

import java.util.Locale.Category;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import model.Persona;
import model.PersonaDAO;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.text.Text;

//Nuestra clase principal debe extender de Application
public class Main extends Application {

	// *********Variables
	private ResourceBundle texts;
	private PersonaDAO taulaPersones;
	FXMLLoader loader;

	// ********Elementos del GUI**********//

	// la etiqueta "@FXML" expone la variable al archivo fxml, lo cual nos permite
	// vincular una variable a un componente

	@FXML
	private Button salirBtn;

	@FXML
	private Text textoText;

	@FXML
	private TextField nombreInput;

	@FXML
	private CheckBox checkBoxEjemplo;

	@FXML
	private TextArea textAreaEjemplo;

	@FXML
	private DatePicker data;
	
	@FXML
	private CheckComboBox<String> box;

	// Esta funcion se ejecuta una vez se haya inicializado, aqui se debe llamar a
	// show
	@Override
	public void start(Stage primaryStage) {
		try {
			// crea la escena
			Scene fm_inici;
			// Carga el generador de escenas
			loader = new FXMLLoader(getClass().getResource("/vista/InicioView.fxml"));

			// guarda la localizacion del sistema operativo
			Locale localitzacioDisplay = Locale.getDefault(Category.DISPLAY);
			// Carga el fichero multi-idioma para la internacionalizacion en base a la
			// localizacion
			texts = ResourceBundle.getBundle("vista.Texts", localitzacioDisplay);
			// Le asigna el titulo y la escena
			fm_inici = new Scene(loader.load());
			primaryStage.setTitle(texts.getString("txt.title"));

			// añade los archivos de internacionalizacion a la escena
			loader.setResources(texts);
			primaryStage.setScene(fm_inici);

			
			
			// inicia la ventana
			primaryStage.show();
			initialize();
			
			box = (CheckComboBox<String>) loader.getNamespace().get("box");
			
			
			//Añadir elementos al checkComboBox, se debe obtener la lista y añadir los elementos 
			box.getItems().add(taulaPersones.buscar(1).getNombre());
			box.getItems().add(taulaPersones.buscar(2).getNombre());
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		launch(args);
	}

	// Esta funcion se ejecuta al inicio del todo
	@FXML
	public void initialize() {
		// declara el valor que tiene el texto
		//textoText.setText("Ingresa un número");
		// generamos valores iniciales
		taulaPersones = new PersonaDAO();
		taulaPersones.guardar(new Persona(1, "Alvaro"));
		taulaPersones.guardar(new Persona(2, "Manuel"));
		taulaPersones.guardar(new Persona(3, "Random"));
		
		
	}

	// ***** Estas funciones las declaramos nosotros, pero para que sea accesibles
	// por el fxml debe tener la etiqueta "@FXML" y recibir un Action
	// event**********/////

	// evento al presionar un boton
	@FXML
	void onActionSalir(ActionEvent event) {
		// capturamos el texto escrito en el input
		String str = nombreInput.getText();
		// seteamos el campo de texto con el string
		textoText.setText(str);
		
		//obtener la lista de elementos seleccionados
		System.out.println("Elementos seleccionados:");
		for (String item : box.getCheckModel().getCheckedItems()) {
			System.out.println(item);
		}
		
	}

	@FXML
	void changeDate(ActionEvent event) {
		textoText.setText(data.getValue().toString());
	}

	// evento que se llama cuando se presiona una tecla
	@FXML
	void onPressedKey(KeyEvent event) {
		// Si la tecla pulsada es espacio o enter
		if (event.getCode() == KeyCode.SPACE || event.getCode() == KeyCode.ENTER) {
			// Buscamos una persona con el id correspondiente
			int id = 0;
			if (nombreInput.getText().matches("[0-9]+")) {
				id = Integer.parseInt(nombreInput.getText());
			}
			Persona persona = taulaPersones.buscar(id);
			if (persona != null) {
				textoText.setText(persona.getNombre());
				// Miramos si un checkbox est� pulsado o no
				if (!checkBoxEjemplo.isSelected()) {
					// Desabilitamos un campo para que no se pueda escribir
					nombreInput.setDisable(true);
				}
				// Marcamos el checkbox
				checkBoxEjemplo.setSelected(true);
			}
		}

		// Si la tecla pulsada es el tabulador
		if (event.getCode() == KeyCode.TAB) {
			// Usamos la funci�n split para recoger los registros del textArea
			String[] listaProductos = textAreaEjemplo.getText().split(",");
			for (String idProducto : listaProductos) {
				System.out.println(idProducto);
			}
		}
	}
}
