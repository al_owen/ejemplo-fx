package model;

import java.util.HashMap;

public class PersonaDAO {
	HashMap<Integer, Persona> listaPersonas = new HashMap<>();
	
	public void guardar(Persona p) {
		listaPersonas.put(p.getId(), p);
	}
	
	public Persona buscar(int id) {
		return listaPersonas.get(id);
	}
}
